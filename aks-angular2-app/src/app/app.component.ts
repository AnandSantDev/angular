import {Component} from '@angular/core';

@Component({
	'selector': 'pm-product',
	'template': `
		<div>
			<h1>Hello {{pageTitle}}</h1>
		</div>
	`
})

export class AppComponent { 
	pageTitle: string = 'Angular 7'	
}